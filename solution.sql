-- a. Find all artists with letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- b. Find all songs that has length < 230
SELECT * FROM songs WHERE length < 230;

-- c. Join 'albums' and 'songs' tables. Only show album_name, song_name and song_length
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- d. Join 'artists' and 'albums' tables. (Find all albums with letter a in its name)
SELECT * FROM albums 
	JOIN artists ON albums.artist_id = artists.id
	WHERE albums.album_title LIKE "%a%";

-- e. Sort the albums in Z-A. Show only 4 records
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. Join 'albums' and 'songs' tables. Sort albums from Z-A and sort songs from A-Z
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY albums.album_title DESC, songs.song_name ASC;



